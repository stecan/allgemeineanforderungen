\babel@toc {ngerman}{}
\contentsline {chapter}{\numberline {1}Status}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Selected for Sprint}{3}{section.1.1}
\contentsline {section}{\numberline {1.2}Assigned}{3}{section.1.2}
\contentsline {section}{\numberline {1.3}Backlog}{3}{section.1.3}
\contentsline {section}{\numberline {1.4}Requirements Elaboration List}{3}{section.1.4}
\contentsline {section}{\numberline {1.5}Finished}{4}{section.1.5}
\contentsline {section}{\numberline {1.6}Statistics}{4}{section.1.6}
\contentsline {chapter}{\numberline {2}Anforderungen an die Dokumentation eines Projektes}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Anforderungen an die Dokumentation}{5}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Anforderungen}{5}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Deutsch}{5}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Dokumentation}{6}{subsection.2.1.3}
\contentsline {subsection}{\numberline {2.1.4}Englisch}{6}{subsection.2.1.4}
\contentsline {subsection}{\numberline {2.1.5}FAQ}{6}{subsection.2.1.5}
\contentsline {subsection}{\numberline {2.1.6}Google Translator}{6}{subsection.2.1.6}
\contentsline {subsection}{\numberline {2.1.7}Howto}{6}{subsection.2.1.7}
\contentsline {subsection}{\numberline {2.1.8}Installation Manual}{7}{subsection.2.1.8}
\contentsline {subsection}{\numberline {2.1.9}Kollaborationsplattform}{7}{subsection.2.1.9}
\contentsline {subsection}{\numberline {2.1.10}Properties File de}{7}{subsection.2.1.10}
\contentsline {subsection}{\numberline {2.1.11}Properties File en}{7}{subsection.2.1.11}
\contentsline {subsection}{\numberline {2.1.12}Sprache}{7}{subsection.2.1.12}
\contentsline {subsection}{\numberline {2.1.13}Twiki}{8}{subsection.2.1.13}
\contentsline {subsection}{\numberline {2.1.14}\IeC {\"U}bersetzungen}{8}{subsection.2.1.14}
\contentsline {subsection}{\numberline {2.1.15}\IeC {\"U}bersetzungstabelle Properties File}{8}{subsection.2.1.15}
\contentsline {subsection}{\numberline {2.1.16}User Manual}{8}{subsection.2.1.16}
\contentsline {subsection}{\numberline {2.1.17}Weitere Sprachen}{9}{subsection.2.1.17}
\contentsline {subsection}{\numberline {2.1.18}rmtoo}{9}{subsection.2.1.18}
