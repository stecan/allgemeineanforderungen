\babel@toc {ngerman}{}
\contentsline {chapter}{\numberline {1}Status}{5}{chapter.1}
\contentsline {section}{\numberline {1.1}Selected for Sprint}{5}{section.1.1}
\contentsline {section}{\numberline {1.2}Assigned}{5}{section.1.2}
\contentsline {section}{\numberline {1.3}Backlog}{5}{section.1.3}
\contentsline {section}{\numberline {1.4}Requirements Elaboration List}{5}{section.1.4}
\contentsline {section}{\numberline {1.5}Finished}{7}{section.1.5}
\contentsline {section}{\numberline {1.6}Statistics}{7}{section.1.6}
\contentsline {chapter}{\numberline {2}Anforderungen an die querschnittliche Belange}{8}{chapter.2}
\contentsline {section}{\numberline {2.1}QuerschnittlicheBelange}{8}{section.2.1}
\contentsline {section}{\numberline {2.2}Anforderungen an die Ausnutzung von Parallelit\IeC {\"a}t}{8}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Parallelit\IeC {\"a}t}{8}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Anforderungen an die Benutzung der Grafikkarte}{9}{subsection.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.2.1}GPU}{9}{subsubsection.2.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2.2}Grafikkarte}{9}{subsubsection.2.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.2.3}OpenCL}{9}{subsubsection.2.2.2.3}
\contentsline {subsubsection}{\numberline {2.2.2.4}OpenGL}{9}{subsubsection.2.2.2.4}
\contentsline {subsubsection}{\numberline {2.2.2.5}Software Anbindung}{10}{subsubsection.2.2.2.5}
\contentsline {subsection}{\numberline {2.2.3}Anforderungen an Stationen}{10}{subsection.2.2.3}
\contentsline {subsubsection}{\numberline {2.2.3.1}Aufgaben-Verteilung}{10}{subsubsection.2.2.3.1}
\contentsline {subsubsection}{\numberline {2.2.3.2}Automatische Verteilung}{10}{subsubsection.2.2.3.2}
\contentsline {subsubsection}{\numberline {2.2.3.3}Bootloader}{11}{subsubsection.2.2.3.3}
\contentsline {subsubsection}{\numberline {2.2.3.4}Draht gebunden}{11}{subsubsection.2.2.3.4}
\contentsline {subsubsection}{\numberline {2.2.3.5}Drahtlos}{11}{subsubsection.2.2.3.5}
\contentsline {subsubsection}{\numberline {2.2.3.6}Ethernet}{11}{subsubsection.2.2.3.6}
\contentsline {subsubsection}{\numberline {2.2.3.7}Mehrere Handys}{12}{subsubsection.2.2.3.7}
\contentsline {subsubsection}{\numberline {2.2.3.8}Mehrere Rechner}{12}{subsubsection.2.2.3.8}
\contentsline {subsubsection}{\numberline {2.2.3.9}Skalierung der Stationen}{12}{subsubsection.2.2.3.9}
\contentsline {subsubsection}{\numberline {2.2.3.10}Skalierungsfaktor Stationen}{12}{subsubsection.2.2.3.10}
\contentsline {subsubsection}{\numberline {2.2.3.11}Stationen}{13}{subsubsection.2.2.3.11}
\contentsline {subsubsection}{\numberline {2.2.3.12}USB}{13}{subsubsection.2.2.3.12}
\contentsline {subsubsection}{\numberline {2.2.3.13}Verbindungswege}{13}{subsubsection.2.2.3.13}
\contentsline {subsubsection}{\numberline {2.2.3.14}Werte gr\IeC {\"o}\IeC {\ss }er oder gleich Eins}{13}{subsubsection.2.2.3.14}
\contentsline {subsubsection}{\numberline {2.2.3.15}Werte kleiner Eins}{13}{subsubsection.2.2.3.15}
\contentsline {subsubsection}{\numberline {2.2.3.16}Werte des Skalierungsfaktors}{14}{subsubsection.2.2.3.16}
\contentsline {subsubsection}{\numberline {2.2.3.17}Wireless 802}{14}{subsubsection.2.2.3.17}
\contentsline {subsection}{\numberline {2.2.4}Anforderungen, um Tasking zu verwenden}{14}{subsection.2.2.4}
\contentsline {subsubsection}{\numberline {2.2.4.1}Abbruch bei Fehler}{14}{subsubsection.2.2.4.1}
\contentsline {subsubsection}{\numberline {2.2.4.2}Abbruch durch Benutzer}{14}{subsubsection.2.2.4.2}
\contentsline {subsubsection}{\numberline {2.2.4.3}Abbruch-Verhalten}{15}{subsubsection.2.2.4.3}
\contentsline {subsubsection}{\numberline {2.2.4.4}Asynchroner Abbruch}{15}{subsubsection.2.2.4.4}
\contentsline {subsubsection}{\numberline {2.2.4.5}Automatische Skalierung Tasks}{15}{subsubsection.2.2.4.5}
\contentsline {subsubsection}{\numberline {2.2.4.6}Rechenzeitintensive Tasks}{15}{subsubsection.2.2.4.6}
\contentsline {subsubsection}{\numberline {2.2.4.7}Skalierung Tasks}{15}{subsubsection.2.2.4.7}
\contentsline {subsubsection}{\numberline {2.2.4.8}Synchroner Abbruch}{16}{subsubsection.2.2.4.8}
\contentsline {subsubsection}{\numberline {2.2.4.9}Tasks}{16}{subsubsection.2.2.4.9}
\contentsline {subsubsection}{\numberline {2.2.4.10}Verwaltungs-Tasks}{16}{subsubsection.2.2.4.10}
\contentsline {section}{\numberline {2.3}Anforderungen an die Performance des Systems}{17}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Performance}{17}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Performance Application}{17}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Performance Client}{17}{subsection.2.3.3}
\contentsline {subsection}{\numberline {2.3.4}Performance Server}{17}{subsection.2.3.4}
\contentsline {section}{\numberline {2.4}Anforderungen an die Kompatibilit\IeC {\"a}t}{18}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Abw\IeC {\"a}rts-Kompatibilit\IeC {\"a}t}{18}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Aufw\IeC {\"a}rts-Kompatibilit\IeC {\"a}t}{18}{subsection.2.4.2}
\contentsline {subsection}{\numberline {2.4.3}Datenkompatibilit\IeC {\"a}t}{18}{subsection.2.4.3}
\contentsline {subsection}{\numberline {2.4.4}Fehler-Kompatibilit\IeC {\"a}t}{19}{subsection.2.4.4}
\contentsline {subsection}{\numberline {2.4.5}Kompatibilit\IeC {\"a}t}{19}{subsection.2.4.5}
\contentsline {subsection}{\numberline {2.4.6}Programmkompatibilit\IeC {\"a}t}{19}{subsection.2.4.6}
\contentsline {section}{\numberline {2.5}Anforderungen an die Speicherverwaltung}{20}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}Komplettes Laden}{20}{subsection.2.5.1}
\contentsline {subsection}{\numberline {2.5.2}Laden bei Bedarf}{20}{subsection.2.5.2}
\contentsline {subsection}{\numberline {2.5.3}Sparsamer Speichergebrauch}{20}{subsection.2.5.3}
\contentsline {subsection}{\numberline {2.5.4}Speicherverwaltung}{21}{subsection.2.5.4}
\contentsline {section}{\numberline {2.6}Anforderungen an den Test}{21}{section.2.6}
\contentsline {subsection}{\numberline {2.6.1}Benutzer Test}{21}{subsection.2.6.1}
\contentsline {subsection}{\numberline {2.6.2}Blackbox Test}{21}{subsection.2.6.2}
\contentsline {subsection}{\numberline {2.6.3}C0 CodeCoverage}{22}{subsection.2.6.3}
\contentsline {subsection}{\numberline {2.6.4}CUnit}{22}{subsection.2.6.4}
\contentsline {subsection}{\numberline {2.6.5}CodeCoverage}{22}{subsection.2.6.5}
\contentsline {subsection}{\numberline {2.6.6}CppUnit}{22}{subsection.2.6.6}
\contentsline {subsection}{\numberline {2.6.7}Debugger}{22}{subsection.2.6.7}
\contentsline {subsection}{\numberline {2.6.8}Integrationstest}{23}{subsection.2.6.8}
\contentsline {subsection}{\numberline {2.6.9}JUnit}{23}{subsection.2.6.9}
\contentsline {subsection}{\numberline {2.6.10}Performance Testing}{23}{subsection.2.6.10}
\contentsline {subsection}{\numberline {2.6.11}Systemtest}{23}{subsection.2.6.11}
\contentsline {subsection}{\numberline {2.6.12}Test}{23}{subsection.2.6.12}
\contentsline {subsection}{\numberline {2.6.13}Testreport}{24}{subsection.2.6.13}
\contentsline {subsection}{\numberline {2.6.14}Testspezifikation}{24}{subsection.2.6.14}
\contentsline {subsection}{\numberline {2.6.15}Toolunterst\IeC {\"u}tzung}{24}{subsection.2.6.15}
\contentsline {subsection}{\numberline {2.6.16}Undokumentiert}{24}{subsection.2.6.16}
\contentsline {subsection}{\numberline {2.6.17}UnitTest}{25}{subsection.2.6.17}
\contentsline {subsection}{\numberline {2.6.18}UnitTestRecord}{25}{subsection.2.6.18}
\contentsline {subsection}{\numberline {2.6.19}Whitebox-Test}{25}{subsection.2.6.19}
\contentsline {section}{\numberline {2.7}Anforderungen an die Sicherheit des Systems}{25}{section.2.7}
\contentsline {subsection}{\numberline {2.7.1}\IeC {\"A}nderung bei Inbetriebnahme}{25}{subsection.2.7.1}
\contentsline {subsection}{\numberline {2.7.2}Benutzer}{26}{subsection.2.7.2}
\contentsline {subsection}{\numberline {2.7.3}Benutzer Admin}{26}{subsection.2.7.3}
\contentsline {subsection}{\numberline {2.7.4}Benutzer Gast}{26}{subsection.2.7.4}
\contentsline {subsection}{\numberline {2.7.5}Default Passwort}{26}{subsection.2.7.5}
\contentsline {subsection}{\numberline {2.7.6}Erzeugung des Default Passwortes}{26}{subsection.2.7.6}
\contentsline {subsection}{\numberline {2.7.7}IT Sicherheit}{27}{subsection.2.7.7}
\contentsline {subsection}{\numberline {2.7.8}Mitre CVE}{27}{subsection.2.7.8}
\contentsline {subsection}{\numberline {2.7.9}Mitre CWE}{27}{subsection.2.7.9}
\contentsline {subsection}{\numberline {2.7.10}OpenSSH}{27}{subsection.2.7.10}
\contentsline {subsection}{\numberline {2.7.11}Passw\IeC {\"o}rter}{27}{subsection.2.7.11}
\contentsline {subsection}{\numberline {2.7.12}Passwort Verwaltung}{28}{subsection.2.7.12}
\contentsline {subsection}{\numberline {2.7.13}Reset Passwort}{28}{subsection.2.7.13}
\contentsline {subsection}{\numberline {2.7.14}Sicherer Code}{28}{subsection.2.7.14}
\contentsline {subsection}{\numberline {2.7.15}Sicherheit}{28}{subsection.2.7.15}
\contentsline {subsection}{\numberline {2.7.16}Verschl\IeC {\"u}sselung}{28}{subsection.2.7.16}
\contentsline {subsection}{\numberline {2.7.17}Zugang}{29}{subsection.2.7.17}
