\babel@toc {ngerman}{}
\contentsline {chapter}{\numberline {1}Status}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Selected for Sprint}{3}{section.1.1}
\contentsline {section}{\numberline {1.2}Assigned}{3}{section.1.2}
\contentsline {section}{\numberline {1.3}Backlog}{3}{section.1.3}
\contentsline {section}{\numberline {1.4}Requirements Elaboration List}{3}{section.1.4}
\contentsline {section}{\numberline {1.5}Finished}{4}{section.1.5}
\contentsline {section}{\numberline {1.6}Statistics}{4}{section.1.6}
\contentsline {chapter}{\numberline {2}Programmentwicklung}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Programmentwicklung}{5}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}3 stellige Versionsnummer}{5}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Android}{5}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}AndroidStudio}{6}{subsection.2.1.3}
\contentsline {subsection}{\numberline {2.1.4}Betriebssysteme}{6}{subsection.2.1.4}
\contentsline {subsection}{\numberline {2.1.5}C}{6}{subsection.2.1.5}
\contentsline {subsection}{\numberline {2.1.6}Cpp}{6}{subsection.2.1.6}
\contentsline {subsection}{\numberline {2.1.7}Git}{6}{subsection.2.1.7}
\contentsline {subsection}{\numberline {2.1.8}Handy}{7}{subsection.2.1.8}
\contentsline {subsection}{\numberline {2.1.9}Hardware}{7}{subsection.2.1.9}
\contentsline {subsection}{\numberline {2.1.10}IDEs}{7}{subsection.2.1.10}
\contentsline {subsection}{\numberline {2.1.11}Java}{7}{subsection.2.1.11}
\contentsline {subsection}{\numberline {2.1.12}Java Styleguide}{7}{subsection.2.1.12}
\contentsline {subsection}{\numberline {2.1.13}Kommentare}{8}{subsection.2.1.13}
\contentsline {subsection}{\numberline {2.1.14}Linux}{8}{subsection.2.1.14}
\contentsline {subsection}{\numberline {2.1.15}Netbeans}{8}{subsection.2.1.15}
\contentsline {subsection}{\numberline {2.1.16}PC}{8}{subsection.2.1.16}
\contentsline {subsection}{\numberline {2.1.17}Programmentwicklung}{9}{subsection.2.1.17}
\contentsline {subsection}{\numberline {2.1.18}Programmiersprachen}{9}{subsection.2.1.18}
\contentsline {subsection}{\numberline {2.1.19}Quellcode}{9}{subsection.2.1.19}
\contentsline {subsection}{\numberline {2.1.20}Referenzierung der Anforderungen}{9}{subsection.2.1.20}
\contentsline {subsection}{\numberline {2.1.21}Referenzierung mit Label Req0000}{10}{subsection.2.1.21}
\contentsline {subsection}{\numberline {2.1.22}Sp\IeC {\"a}rliche Kommentare}{10}{subsection.2.1.22}
\contentsline {subsection}{\numberline {2.1.23}Styleguide}{10}{subsection.2.1.23}
\contentsline {subsection}{\numberline {2.1.24}Versionsnummer}{10}{subsection.2.1.24}
\contentsline {subsection}{\numberline {2.1.25}Versionsverwaltung}{11}{subsection.2.1.25}
